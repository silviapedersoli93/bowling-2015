note
	description: "Summary description for {GAME_IMPLEMENTATION}."
	author: "Mattia Monga"

class
	GAME_IMPLEMENTATION

inherit

	GAME

feature

	score: INTEGER
		do
			Result := my_score
		end

	roll (pins: INTEGER)
		do
			current_roll := current_roll + 1
			my_score := my_score + pins
		%	print ("il risultato:")
		%	print (my_score)
		end

	ended: BOOLEAN
		do
			if current_roll = 20 then
				Result := True
			end
		end


feature {NONE}

	current_roll: INTEGER
	my_score: INTEGER

invariant
	valid_roll: 0 <= current_roll and current_roll <= 20

end
